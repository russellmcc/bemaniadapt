use cortex_m::asm::delay;
use embedded_hal::blocking::spi::Transfer;

pub struct DelaySPI<SPI> {
    spi: SPI,
}

impl<SPI: Transfer<u8>> DelaySPI<SPI> {
    pub fn new(spi: SPI) -> DelaySPI<SPI> {
        DelaySPI{spi}
    }
}

impl<SPI: Transfer<u8>> Transfer<u8>
    for DelaySPI<SPI>
{
    type Error = SPI::Error;

    fn transfer<'w>(&mut self, words: &'w mut [u8]) -> Result<&'w [u8], Self::Error> {
        for mut word in words.iter_mut() {
            self.spi.transfer(core::slice::from_mut(&mut word))?;

            // JRMTODO - replace this hard-coded delay with an ACK signal.
            delay(72 * 4)
        }

        Ok(words)
    }
}