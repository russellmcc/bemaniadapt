use stm32f0xx_hal as hal;

use crate::delay_spi::DelaySPI;
use crate::ps2pad::GamePad;
use cortex_m::asm::delay;
use hal::gpio::gpiob::*;
use hal::gpio::*;
use hal::spi;
use hal::timers;
use hal::usb::{Peripheral, UsbBusType};
use hal::{pac, prelude::*};
use keyberon::hid::HidClass;
use usb_device::bus::UsbBusAllocator;
use usb_device::prelude::{UsbDeviceBuilder, UsbVidPid};

pub type PortSPI = DelaySPI<
    spi::Spi<
        pac::SPI1,
        PB3<Alternate<AF0>>,
        PB4<Alternate<AF0>>,
        PB5<Alternate<AF0>>,
        spi::EightBit,
    >,
>;

pub type PortCS = gpioa::PA8<Output<PushPull>>;

pub type UsbDevice = usb_device::device::UsbDevice<'static, UsbBusType>;
pub type UsbClass = HidClass<'static, UsbBusType, GamePad<PortSPI, PortCS>>;
pub type Timer = timers::Timer<hal::pac::TIM3>;

pub struct Hardware {
    pub usb_dev: UsbDevice,
    pub usb_class: UsbClass,
    pub timer: Timer,
}

const MODE: spi::Mode = spi::Mode {
    polarity: spi::Polarity::IdleHigh,
    phase: spi::Phase::CaptureOnSecondTransition,
};

pub fn init_device(
    mut device: pac::Peripherals,
    usb_bus_maker: &'static mut Option<UsbBusAllocator<UsbBusType>>,
) -> Hardware {
    cortex_m::interrupt::free(move |cs| {
        let mut clocks = device
            .RCC
            .configure()
            .hsi48()
            .enable_crs(device.CRS)
            .sysclk(48.mhz())
            .pclk(24.mhz())
            .freeze(&mut device.FLASH);

        let gpioa = device.GPIOA.split(&mut clocks);
        let gpiob = device.GPIOB.split(&mut clocks);

        // // Simulate USB disconnect/reconnect
        let mut usb_dp = gpioa.pa12.into_push_pull_output(cs);
        usb_dp.set_low().ok();
        delay(480_000);

        let mut timer = timers::Timer::tim3(device.TIM3, 400.hz(), &mut clocks);
        timer.listen(timers::Event::TimeOut);

        let port = pscontroller_rs::PlayStationPort::new(
            DelaySPI::new(spi::Spi::spi1(
                device.SPI1,
                (
                    gpiob.pb3.into_alternate_af0(cs),
                    gpiob.pb4.into_alternate_af0(cs),
                    gpiob.pb5.into_alternate_af0(cs),
                ),
                MODE,
                250_000.hz(),
                &mut clocks,
            )),
            Some(gpioa.pa8.into_push_pull_output(cs)),
        )
        .unwrap();

        let usb = Peripheral {
            usb: device.USB,
            pin_dm: gpioa.pa11,
            pin_dp: usb_dp.into_floating_input(cs),
        };
        *usb_bus_maker = Some(UsbBusType::new(usb));
        let usb_bus = usb_bus_maker.as_ref().unwrap();
        let game_pad = GamePad::new(port);
        let usb_class = HidClass::new(game_pad, &usb_bus);
        let usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
            .manufacturer("Fake company 2")
            .product("My Gamepad Z")
            .serial_number("TEST")
            .build();
        Hardware {
            usb_dev,
            usb_class,
            timer,
        }
    })
}
