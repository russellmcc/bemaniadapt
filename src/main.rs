#![no_std]
#![no_main]
#![warn(nonstandard_style, rust_2018_idioms, future_incompatible, clippy::all)]

#[allow(unused_extern_crates)]
extern crate panic_halt;

mod delay_spi;
mod hardware;
mod ps2pad;

use embedded_hal::timer::CountDown;
use hardware::init_device;
use stm32f0xx_hal::usb::UsbBusType;
use usb_device::bus::UsbBusAllocator;
use usb_device::class::UsbClass;

#[rtic::app(device = stm32f0xx_hal::pac, peripherals = true, monotonic = rtic::cyccnt::CYCCNT)]
const APP: () = {
    struct Resources {
        usb_dev: hardware::UsbDevice,
        usb_class: hardware::UsbClass,
        timer: hardware::Timer,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        static mut USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None;
        let hardware = init_device(cx.device, USB_BUS);
        init::LateResources {
            usb_dev: hardware.usb_dev,
            usb_class: hardware.usb_class,
            timer: hardware.timer,
        }
    }

    #[task(
            binds = TIM3,
            priority = 2,
            resources = [timer, usb_class],
        )]
    fn tick(c: tick::Context) {
        c.resources.timer.wait().ok();
        if let Some(x) = c.resources.usb_class.device_mut().poll() {
            c.resources.usb_class.write(&x).unwrap();
        }
    }

    #[task(binds = USB, priority = 2, resources = [usb_dev, usb_class])]
    fn usb_tx(mut c: usb_tx::Context) {
        usb_poll(&mut c.resources.usb_dev, &mut c.resources.usb_class);
    }
};

fn usb_poll(usb_dev: &mut hardware::UsbDevice, class: &mut hardware::UsbClass) {
    if usb_dev.poll(&mut [class]) {
        class.poll();
    }
}
