use embedded_hal::blocking::spi;
use embedded_hal::digital::v2::OutputPin;
use keyberon::hid::{HidDevice, Protocol, ReportType, Subclass};
use pscontroller_rs::{Device, PlayStationPort};

pub struct GamePad<SPI, CS> {
    port: PlayStationPort<SPI, CS>,
    report: [u8; 4],
}

fn poll_raw<E, SPI: spi::Transfer<u8, Error = E>, CS: OutputPin>(
    port: &mut PlayStationPort<SPI, CS>,
) -> [u8; 4] {
    if let Ok(Device::Classic(x)) = port.read_input(None) {
        let buttons = x.buttons;
        [
            if buttons.left() {
                0x00
            } else if buttons.right() {
                0xFF
            } else {
                0x7F
            },
            if buttons.down() {
                0x00
            } else if buttons.up() {
                0xFF
            } else {
                0x7F
            },
            (if buttons.circle() { 0x01 } else { 0x00 })
                | (if buttons.cross() { 0x02 } else { 0x00 })
                | (if buttons.square() { 0x04 } else { 0x00 })
                | (if buttons.triangle() { 0x08 } else { 0x00 })
                | (if buttons.start() { 0x10 } else { 0x00 })
                | (if buttons.select() { 0x20 } else { 0x00 })
                | (if buttons.l1() { 0x40 } else { 0x000 })
                | (if buttons.r1() { 0x80 } else { 0x00 }),
            (if buttons.l2() { 0x01 } else { 0x00 })
                | (if buttons.r2() { 0x02 } else { 0x00 })
                | (if buttons.l3() { 0x04 } else { 0x00 })
                | (if buttons.r3() { 0x08 } else { 0x00 }),
        ]
    } else {
        [0x7F, 0x7F, 0x00, 0x00]
    }
}

impl<E, SPI: spi::Transfer<u8, Error = E>, CS: OutputPin> GamePad<SPI, CS> {
    pub fn new(port: PlayStationPort<SPI, CS>) -> GamePad<SPI, CS> {
        GamePad {
            port,
            report: [0x7F, 0x7F, 0x00, 0x00],
        }
    }

    pub fn poll(&mut self) -> Option<[u8; 4]> {
        self.report = poll_raw(&mut self.port);
        Some(self.report)
    }
}

const REPORT_DESCRIPTOR: &[u8] = &[
    0x05, 0x01, // Usage Page (Generic Desktop Ctrls)
    0x09, 0x05, // Usage (Joystick)
    0xA1, 0x01, // Collection (Application)
    0xA1, 0x02, //   Collection (Logical)
    0x75, 0x08, //     Report Size (8)
    0x95, 0x02, //     Report Count (2)
    0x15, 0x00, //     Logical Minimum (0)
    0x26, 0xFF, 0x00, //     Logical Maximum (255)
    0x35, 0x00, //     Physical Minimum (0)
    0x46, 0xFF, 0x00, //     Physical Maximum (255)
    0x09, 0x30, //     Usage (X)
    0x09, 0x31, //     Usage (Y)
    0x81, 0x02, //     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    0x75, 0x01, //     Report Size (1)
    0x95, 0x0C, //     Report Count (12)
    0x25, 0x01, //     Logical Maximum (1)
    0x45, 0x01, //     Physical Maximum (1)
    0x05, 0x09, //     Usage Page (Button)
    0x19, 0x01, //     Usage Minimum (0x01)
    0x29, 0x0C, //     Usage Maximum (0x0C)
    0x81, 0x02, //     Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    // Padding
    0x75, 0x04, //     Report Size (4)
    0x95, 0x01, //     Report Count (1)
    0x81, 0x03, //     Input (Const,Var,Abs)
    0xC0, //   End Collection
    0xA1, 0x02, //   Collection (Logical)
    0x75, 0x08, //     Report Size (8)
    0x95, 0x04, //     Report Count (4)
    0x46, 0xFF, 0x00, //     Physical Maximum (255)
    0x26, 0xFF, 0x00, //     Logical Maximum (255)
    0x09, 0x02, //     Usage (0x02)
    0x91,
    0x02, //     Output (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position,Non-volatile)
    0xC0, //   End Collection
    0xC0, // End Collection
];

impl<E, SPI: spi::Transfer<u8, Error = E>, CS: OutputPin> HidDevice for GamePad<SPI, CS> {
    fn subclass(&self) -> Subclass {
        Subclass::None
    }

    fn protocol(&self) -> Protocol {
        Protocol::None
    }

    fn report_descriptor(&self) -> &[u8] {
        REPORT_DESCRIPTOR
    }

    fn get_report(&mut self, report_type: ReportType, _report_id: u8) -> Result<&[u8], ()> {
        match report_type {
            ReportType::Input => {
                self.poll();
                Ok(&self.report)
            }
            _ => Err(()),
        }
    }

    fn set_report(
        &mut self,
        _report_type: ReportType,
        _report_id: u8,
        _data: &[u8],
    ) -> Result<(), ()> {
        Ok(())
    }
}
